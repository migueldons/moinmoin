# MoinMoin

Initial version of Desktop MoinMoin wiki.



Git project to store the first version of MoinMoin with Macros and initial theme.

To run:

```
miguel@miguel-Inspiron-13-7353:/srv/moin-1.9.10$ sudo python wikiserver.py
[sudo] password for miguel: 
2020-03-27 10:50:06,534 INFO MoinMoin.log:135 
```

Now, you should be able to reach from your web program doing:

```
localhost:8080
```