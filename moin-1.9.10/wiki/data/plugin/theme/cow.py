# -*- coding: iso-8859-1 -*-
"""
    cow theme

    Based on MoinMoin - Modern theme

    @copyright: 2003-2005 Nir Soffer, Thomas Waldmann
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin.theme.modern import Theme as modern

class Theme(modern):
    name = u"cow"

    def send_title(self,text,**keywords):
	keywords['html_head'] = '\n'.join([
	    self.externalScript('xmlrpc'),
	    self.externalScript('cow')
	    ])
	# TODO find a way to distinguish editing-action from view-action less "crappy" than this one
        if self.request.url.find('edit') < 0 and self.request.form.get('button_preview','') == '':
	    js_function_name = u"validate('" + self.request.page.page_name + u"');"
	    keywords['body_onload'] = js_function_name

        modern(self.request).send_title(text,**keywords)

def execute(request):
    """
    Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)
