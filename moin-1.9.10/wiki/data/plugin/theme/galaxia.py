# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Loic Vaumerel <http://www.gnewsense.org/LoicVaumerel>
# Copyright (c) 2013 Luis Felipe Lopez Acevedo <felipe.lopac@gmail.com>.
# Copyright (c) 2012 MoinMoin team.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
    MoinMoin - Galaxia theme
    
    @copyright: 2012 Luis Felipe Lopez Acevedo, aka sirgazil
    @license: GNU GPL, see COPYING for details.
"""

from MoinMoin.config import url_prefix_static
from MoinMoin.i18n import getText, wikiLanguages
from MoinMoin.theme import ThemeBase


class Theme(ThemeBase):

    name = "galaxia"
    
    # fake _ function to get gettext recognize those texts:
    _ = lambda x: x

    # TODO: remove icons that are not used any more.
    icons = {
        # key           alt                        icon filename           w   h
        # -------------------------------------------------------------------------
        # navibar
        'help':         ("%(page_help_contents)s", "moin-help.png",        16, 16),
        'find':         ("%(page_find_page)s",     "moin-search.png",      16, 16),
        'diff':         (_("Diffs"),               "moin-diff.png",        16, 16),
        'info':         (_("Info"),                "moin-info.png",        16, 16),
        'edit':         (_("Edit"),                "moin-edit.png",        16, 16),
        'unsubscribe':  (_("Unsubscribe"),         "moin-unsubscribe.png", 16, 16),
        'subscribe':    (_("Subscribe"),           "moin-subscribe.png",   16, 16),
        'raw':          (_("Raw"),                 "moin-raw.png",         16, 16),
        'xml':          (_("XML"),                 "moin-xml.png",         20, 13),
        'print':        (_("Print"),               "moin-print.png",       16, 16),
        'view':         (_("View"),                "moin-show.png",        16, 16),
        'home':         (_("Home"),                "moin-home.png",        16, 16),
        'up':           (_("Up"),                  "moin-parent.png",      16, 16),
        # FileAttach
        'attach':       ("%(attach_count)s",       "moin-attach.png",      16, 16),
        'attachimg':    ("",                       "attach.png",           32, 32),
        # RecentChanges
        'rss':          (_("[RSS]"),               "moin-rss.png",         16, 16),
        'deleted':      (_("[DELETED]"),           "moin-deleted.png",     16, 16),
        'updated':      (_("[UPDATED]"),           "moin-updated.png",     16, 16),
        'renamed':      (_("[RENAMED]"),           "moin-renamed.png",     16, 16),
        'conflict':     (_("[CONFLICT]"),          "moin-conflict.png",    16, 16),
        'new':          (_("[NEW]"),               "moin-new.png",         16, 16),
        'diffrc':       (_("[DIFF]"),              "moin-diff.png",        16, 16),
        # General
        'bottom':       (_("[BOTTOM]"),            "moin-bottom.png",      16, 16),
        'top':          (_("[TOP]"),               "moin-top.png",         16, 16),
        'www':          ("[WWW]",                  "moin-www.png",         16, 16),
        'mailto':       ("[MAILTO]",               "moin-email.png",       16, 16),
        'news':         ("[NEWS]",                 "moin-news.png",        16, 16),
        'telnet':       ("[TELNET]",               "moin-telnet.png",      16, 16),
        'ftp':          ("[FTP]",                  "moin-ftp.png",         16, 16),
        'file':         ("[FILE]",                 "moin-ftp.png",         16, 16),
        # search forms
        'searchbutton': ("[?]",                    "moin-search.png",      16, 16),
        'interwiki':    ("[%(wikitag)s]",          "moin-inter.png",       16, 16),

        # smileys (this is CONTENT, but good looking smileys depend on looking
        # adapted to the theme background color and theme style in general)
        #vvv    ==      vvv  this must be the same for GUI editor converter
        'X-(':          ("X-(",                    'angry.png',            16, 16),
        ':D':           (":D",                     'biggrin.png',          16, 16),
        '<:(':          ("<:(",                    'frown.png',            16, 16),
        ':o':           (":o",                     'redface.png',          16, 16),
        ':(':           (":(",                     'sad.png',              16, 16),
        ':)':           (":)",                     'smile.png',            16, 16),
        'B)':           ("B)",                     'smile2.png',           16, 16),
        ':))':          (":))",                    'smile3.png',           16, 16),
        ';)':           (";)",                     'smile4.png',           16, 16),
        '/!\\':         ("/!\\",                   'alert.png',            16, 16),
        '<!>':          ("<!>",                    'attention.png',        16, 16),
        '(!)':          ("(!)",                    'idea.png',             16, 16),

        # copied 2001-11-16 from http://pikie.darktech.org/cgi/pikie.py?EmotIcon
        ':-?':          (":-?",                    'tongue.png',           16, 16),
        ':\\':          (":\\",                    'ohwell.png',           16, 16),
        '>:>':          (">:>",                    'devil.png',            16, 16),
        '|)':           ("|)",                     'tired.png',            16, 16),

        # some folks use noses in their emoticons
        ':-(':          (":-(",                    'sad.png',              16, 16),
        ':-)':          (":-)",                    'smile.png',            16, 16),
        'B-)':          ("B-)",                    'smile2.png',           16, 16),
        ':-))':         (":-))",                   'smile3.png',           16, 16),
        ';-)':          (";-)",                    'smile4.png',           16, 16),
        '|-)':          ("|-)",                    'tired.png',            16, 16),

        # version 1.0
        '(./)':         ("(./)",                   'checkmark.png',        16, 16),
        '{OK}':         ("{OK}",                   'thumbs-up.png',        16, 16),
        '{X}':          ("{X}",                    'icon-error.png',       16, 16),
        '{i}':          ("{i}",                    'icon-info.png',        16, 16),
        '{1}':          ("{1}",                    'prio1.png',            15, 13),
        '{2}':          ("{2}",                    'prio2.png',            15, 13),
        '{3}':          ("{3}",                    'prio3.png',            15, 13),

        # version 1.3.4 (stars)
        # try {*}{*}{o}
        '{*}':          ("{*}",                    'star_on.png',          16, 16),
        '{o}':          ("{o}",                    'star_off.png',         16, 16),
    }
    del _
    
    def header(self, d, **kw):
        """ Assemble wiki header

        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        html = [
            # Pre header custom html
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<div id="header">',
            self.logo(d),
            self.human_menu(d),
            self.searchform(d),
            self.username(d),
            u'<div id="locationline">',
            self.interwiki(d),
            #self.title(d),
            u'</div>',
            self.breadcrumbs(d),
            #self.navibar(d),
            #self.trail(d),
            #u'<hr id="pageline">',
            u'<div id="pageline"><hr style="display:none;"></div>',
            self.msg(d),
            self.editbar(d),
            self.language_widget(d),
            u'</div>',

            # Post header custom html (not recommended)
            self.emit_custom_html(self.cfg.page_header2),

            # Start of page
            self.startPage(),
        ]
        return u'\n'.join(html)
    
    def send_title(self, text, **keywords):
        """ Return page HTML header with the title specified by PRAGMA title.
        
        This method overrides ``MoinMoin.theme.ThemeBase.send_title()``, so
        that the ``title`` element of the HTML page is updated with the title
        specified by the ``#PRAGMA title`` instruction in wiki syntax.
        
        This is used to provide a localized, human friendly title for the
        requested page.
        
        """
        text = self.request.getPragma('title', text)
        return ThemeBase.send_title(self, text, **keywords)
    
    def get_gns_langcode(self, lang_code):
        """Return ``lang_code`` formated in gNewSense website style.
        
        This method takes a language code as used in MoinMoin/i18n language
        catalogs (e.g. pt-br) and reformat it to the style used for language
        codes in gNewSense website URLs (e.g. pt_BR).
        
        """
        if "-" in lang_code:
            split_code = lang_code.split("-", 1)
            split_code[1] = split_code[1].upper()
            lang_code = "%s_%s" % (split_code[0], split_code[1])
            
        return lang_code

    def get_moin_langcode(self, lang_code):
        """Return ``lang_code`` formated in MoinMoin style.
        
        This method takes a language code as used in gNewSense website URLs
        (e.g. pt_BR) and reformat it to the style used for MoinMoin/i18n
        language catalogs (e.g. pt-br).
        
        """
        lang_code = lang_code.lower()
        lang_code = lang_code.replace("_", "-")
            
        return lang_code
    
    def get_page_langcode(self, d, **kw):
        """ Return the language code of the requested page.
        
        This method search for a language code in the path of the requested
        page and returns it as a string.
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: string representing the language code of the page
        """
        # Get the first section of the page path
        page_path = d['page'].page_name
        section = page_path.split('/')[0]
                
        # Check if section is a language code
        if self.get_moin_langcode(section) in wikiLanguages():
            # It is a language code
            return section
        else:
            # It is a page written in the default language and does not have
            # a language code.
            return self.cfg.language_default
    
    def translate_text(self, text, lang_code):
        """ Return text translated to the language indicated by lang_code.
        
        This method translates additional text for the human_menu,
        breadcrumbs, and language_widget introduced by Galaxia theme.
        
        @param text: the English text to translate
        @param lang_code: a language code as used in gNewSense (e.g. pt_BR)
        @rtype: unicode
        @return: translated text or the same text if no translation found
        """
        try:
            # Search for a translation in MoinMoin/i18n/lang_code.MoinMoin.po.
            # If no translation is found for the text, search for additional
            # translations defined in a page dictionary <Language>Dict
            # (see http://moinmo.in/HelpOnDictionaries).
            return getText(text, self.request, self.get_moin_langcode(lang_code))
        except IOError:
            # The requested language is not supported by MoinMoin yet.
            # gNewSense translators should add the unsupported language to
            # http://moinmo.in/.
            return text
    
    def translate_path(self, path, lang_code):
        """ Return page path localized to lang_code.
        
        This method takes a page path and translates it to the language
        indicated by lang_code. For example, if path is
        
            es/Documentation/InstallationManual
        
        and the lang_code is "zh_TW", the returned path will be
        
            zh_TW/Documentation/InstallationManual
        
        @param path: a MoinMoin page path
        @param lang_code: gNewSense URL style language code (e.g. zh_TW)
        @rtype: unicode
        @return: translated path
        """
        generic_path = u'#'
        
        # Remove any language code from the path
        section = path.split('/')[0]
        
        # Check if section is a language code
        if self.get_moin_langcode(section) in wikiLanguages():
            # Remove section from path because it is a language code
            target = u'%s/' % section
            generic_path = path.replace(target, '')
        else:
            # Path does not contain a language code, so it is generic
            generic_path = path
        
        # Translate the path
        if lang_code == self.cfg.language_default:
            # Return a path without language code in it
            return generic_path
        else:
            return u'%s/%s' % (lang_code, generic_path)

    def logo(self, d, **kw):
        """ Assemble logo with link to localized front page

        The logo contains an image and or text or any html markup the
        admin inserted in the config file. Everything it enclosed inside
        a div with id="logo".
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: logo html
        """
        html = u''
        
        # Get the language code of the requested page
        lang_code = self.get_page_langcode(d)
        
        # Assemble Home link
        link_href = self.translate_path(self.cfg.page_front_page, lang_code)
        
        if self.cfg.logo_string:
            home_link = '<a href="/%s">%s</a>' % (link_href, self.cfg.logo_string)
            html = u'<div id="logo">%s</div>' % home_link
        
        return html
    
    def human_menu(self, d, **kw):
        """ Assemble easy to read main menu for the header
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: page main menu html
        """
        # Get the language code of the requested page
        lang_code = self.get_page_langcode(d)
        
        # Update links according to the language code
        download_text = self.translate_text('Index', lang_code)
        download_href = self.translate_path('Main/Index', lang_code)
        doc_text = self.translate_text('DOU University', lang_code)
        doc_href = self.translate_path('DOU University', lang_code)
        support_text = self.translate_text('Zoho', lang_code)
        support_href = self.translate_path('Zoho', lang_code)
        dev_text = self.translate_text('Help this wiki!', lang_code)
        dev_href = self.translate_path('Main/Help this wiki!', lang_code)
        
        html = [
            u'<ul id="human-menu">',
            u'<li><a href="/%s">%s</a></li>' % (download_href, download_text),
            u'<li><a href="/%s">%s</a></li>' % (doc_href, doc_text),
            u'<li><a href="/%s">%s</a></li>' % (support_href, support_text),
            u'<li><a href="/%s">%s</a></li>' % (dev_href, dev_text),
            u'</ul>',
        ]
        
        return u'\n'.join(html)
    
    def breadcrumbs(self, d, **kw):
        """ Assemble breadcrumbs according to a page path.
        
        This method takes the page path to the requested page and break the
        path into links in a breadcrumbs-like fashion.
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: breadcrumbs in html format
        """
        # Assemble path-root href according to page language
        lang_code = self.get_page_langcode(d)
        
        if lang_code == self.cfg.language_default:
            path_root_href = '/%s' % self.cfg.page_front_page
        else:
            path_root_href = '/%s/%s' % (lang_code, self.cfg.page_front_page)
        
        # Assemble breadcrumbs
        html = [
            u'<div id="breadcrumbs">',
            u'<a id="path-root" href="%s">' % path_root_href,
            u'<img src="%s/galaxia/img/path-root.png" alt="Root">' % url_prefix_static,
            u'</a>',
        ]
        
        pages = d['page'].page_name.split('/')
        for page in pages:
            link = d['page'].page_name.partition(page)[0] + page
            html += [
                u'<a class="path-child" href="/' + link + '">' + page + '</a>',
            ]

        html += [
            u'</div>',
        ]
        
        return u'\n'.join(html)
    
    def editorheader(self, d, **kw):
        """ Assemble wiki header for editor

        @param d: parameter dictionary
        @rtype: unicode
        @return: page header html
        """
        html = [
            # Pre header custom html
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<div id="header">',
            self.logo(d),
            self.human_menu(d),
            self.searchform(d),
            self.username(d),
            self.msg(d),
            u'</div>',

            # Post header custom html (not recommended)
            self.emit_custom_html(self.cfg.page_header2),

            # Start of page
            self.startPage(),
        ]
        return u'\n'.join(html)


    def language_widget(self, d, **kw):
        """ Assemble a custom language widget.
        
        This widget allows users to select translations available for the
        requested page.
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: language widget in html
        """
        # Start language widget container
        page_lang = self.get_page_langcode(d)
        
        html = [
            u'<div id="lang-widget">',
            u'<div id="lang-list">',
            u'<div>%s</div>' % self.translate_text('Translations', page_lang),
        ]
        
        # Assemble language links
        page_path = d['page'].page_name
        wiki_languages = wikiLanguages()
        
        
        for lang_code in sorted(wikiLanguages()):
            link_href = self.translate_path(
                page_path,
                self.get_gns_langcode(lang_code)
            )
            link_text = wiki_languages[lang_code]['x-language']
            html += [
                u'<a href="/%s">%s</a>' % (link_href, link_text)
            ]
        
        # Close language widget container
        html += [
            u'</div>',
            u'</div>',
        ]
        
        return u'\n'.join(html)   

    def gns_footer(self, d, **kw):
        """ Assemble a custom gNewSense footer.
        
        @param d: parameter dictionary
        @rtype: unicode
        @return: gNewSense footer in html
        """        
        html = [
            u'<div id="gns-footer">',
            u'<p>',
            u' Thanks to gNewSense for theme base provided. All logos ',
            u' and brands shown here are authorized ONLY for this wiki demo. ',
            u' Questions? Get in touch: miguel.hernandez@digitalonus.com'
            u'</p>',
            u'</div>',
        ]
        
        return u'\n'.join(html)


    def footer(self, d, **keywords):
        """ Assemble wiki footer

        @param d: parameter dictionary
        @keyword ...:...
        @rtype: unicode
        @return: page footer html
        """
        page = d['page']
        html = [
            # End of page
            self.pageinfo(page),
            self.endPage(),

            # Pre footer custom html (not recommended!)
            self.emit_custom_html(self.cfg.page_footer1),

            # Footer
            u'<div id="footer">',
            self.editbar(d),
            self.gns_footer(d),
            self.credits(d),
            self.showversion(d, **keywords),
            u'</div>',

            # Post footer custom html
            self.emit_custom_html(self.cfg.page_footer2),
            ]
        return u'\n'.join(html)


def execute(request):
    """
    Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)

