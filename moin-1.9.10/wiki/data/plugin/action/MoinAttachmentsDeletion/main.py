'''
main.py -- actual action entry point.

----
Copyright (C) 2007-2008 Zoran Isailovski
Licensed under the Apache License, Version 2.0 (see http://www.apache.org/licenses/LICENSE-2.0)
or, at your option, under the terms described in the file LICENSE.txt found in the root of the 
distribution of this package (as far as it denotes a different license then the above).
'''

__TODO__ = '''\
'''

DEBUG = True

#-- configuration ------------------------------------------------------------

TITLE = 'Attachments deletion'

#-----------------------------------------------------------------------------

import os, re, fnmatch, glob
import tmpl
from MoinMoin import wikiutil

#-----------------------------------------------------------------------------

def execute(process, pagename, request):
    _ = request.getText
    request.theme.send_title(_(TITLE + ' for "%s"' % pagename))
    try:
        try:
            process(request)
        except:
            import sys, traceback
            output = traceback.format_exception( *sys.exc_info() )
            if not DEBUG: output = output[-1:]
            output = ''.join( output )
            request.write( ERROR_FORMAT % (TITLE, pagename, output) )
    finally:
        request.theme.send_footer(pagename)

ERROR_FORMAT = '''\
<div class="traceback" style="background-color:red; color:lightyellow; font-weight: bold">
<center>%s on %s</center>
<pre class="traceback" style="background-color:yellow; color:red; font: 11px verdana; border: solid red">\
%s\
</pre>
</div>
'''


######################################################################
## 
## Actions
## 
######################################################################

ANY_FILE_PATTERN = re.compile( fnmatch.translate('*.*') )

def CleanupAttachmentsAction(request):
    do(request, ANY_FILE_PATTERN, action='CleanupAttachments', title='Attachments')

######################################################################

GRAPHVIZ_FILE_PATTERN = re.compile( r'graphviz-\w+-\w{40}\.(?:png|gif|jpg)$' )

def GraphVizCleanupAction(request):
    do(request, GRAPHVIZ_FILE_PATTERN, action='GraphVizCleanup', title='GraphViz Attachments')

######################################################################
## 
## Implementation
## 
######################################################################

class struct: pass

def do(request, fnpat, **variables):
    write = request.write
    data = struct() ; vars(data).update(variables)
    fill(data, request, fnpat)
    s = tmpl.renderThis('main', **vars(data))
    write(s)

def fill(data, request, fnpat):
    page = request.page
    folder = page.getPagePath('attachments')
    items = [i for i in request.form.items() if i[0] != 'action']
    messages = []
    ##request.write('<pre>%s</pre>' % items)
    if items:
        for (fname, states) in items:
            ## if states != [u'on']: continue # this does not happen
            if not fnpat.match(fname):
                messages.append('File name mismatch:\n  <b>%s</b>' % fname)
                continue
            path = os.path.join(folder, fname)
            if not os.path.isfile(path):
                messages.append('File not found:\n  <b>%s</b>' % fname)
                continue
            os.remove(path)
            messages.append('File deleted:\n  <b>%s</b>' % fname)

    data.messages = messages
    data.folder = folder
    data.filenames = [ fname for fname in os.listdir(folder) if fnpat.match(fname) ]
    ##data.succ = lambda n, d=1 : n + d
