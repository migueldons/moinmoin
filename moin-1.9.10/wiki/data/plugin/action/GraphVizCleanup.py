'''
Action module as seen by MoinMoin. It delegates all the work to the action package,
optionally reloading it before invokation (usefull during development).

----
Copyright (C) 2007-2008 Zoran Isailovski
Licensed under the Apache License, Version 2.0 (see http://www.apache.org/licenses/LICENSE-2.0)
or, at your option, under the terms described in the file LICENSE.txt found in the root of the 
distribution of this package (as far as it denotes a different license then the above).
'''
from MoinAttachmentsDeletion import main

def execute(pagename, request):
    if main.DEBUG: reload(main)
    return main.execute(main.GraphVizCleanupAction, pagename, request)
