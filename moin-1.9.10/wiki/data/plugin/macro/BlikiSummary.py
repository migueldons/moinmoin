 # -*- coding: iso-8859-1 -*-

"""
 Macro: BlikiSummary
 Abridged RecentChanges-like summary of a blog-wiki (bliki) page

 Version:
 2.0 / 04.12.2008 (for MoinMoin 1.8) 
    Using the arg parser, output using now rawHTML formatter, created a div 
    "BlikiSummary" around the output... (MarcelH�fner)

 Syntax:
 <<BlikiSummary(numberOfChanges=10,numberOfDays=7)>>
 <<BlikiSummary(10,7)>>
 <<BlikiSummary>>

 Copyright (C) 2007 Boris Smus <boris@z3.ca>
 Licence: GPL [see http://www.gnu.org/copyleft/gpl.html]
"""

from MoinMoin.logfile import editlog
from MoinMoin import wikiutil
from MoinMoin.Page import Page
import time

def macro_BlikiSummary(macro, numberOfChanges=12, numberOfDays=14 ):
    request = macro.request
    _ = request.getText
    log = editlog.EditLog(request)
    now = long(time.time())

    pages = []
    for page in log.reverse():
        if not request.user.may.read(page.pagename):
            # ignore pages that aren't readable
            continue
        
        # last edited time property
        edit_time = wikiutil.version2timestamp(page.ed_time_usecs)

        # the day modification of 
        page.edit_diff = time_diff(now, edit_time)
        if page.edit_diff[0] > numberOfDays:
            # all further pages are older
            break
        if Page(request, page.pagename).exists() and \
            page.pagename not in [p.pagename for p in pages]:
            # if this page was edited today, it exists and we haven't yet
            # seen it
            pages.append(page)
            if len(pages) >= numberOfChanges:
                break
            prev_pagename = page.pagename

    #return `[page.pagename for page in pages]`
    html = []
    html.append('<div class="BlikiSummary">')
    html.append(macro.formatter.number_list(1))
    for page in pages:
        html.append(macro.formatter.listitem(1))
        html.append(macro.formatter.pagelink(1, page.pagename, generated=1))
        html.append(macro.formatter.text(page.pagename))
        html.append(macro.formatter.pagelink(0))
        diff = page.edit_diff
        html.append(macro.formatter.text(_(" changed ")))
        if diff[0] != 0:
            # more than a day ago
            html.append(macro.formatter.text(_(" %d %s ") % 
                (diff[0], (diff[0] > 1 and _('days') or _('day')))))
        if diff[1] != 0:
            html.append(macro.formatter.text(_(" %dh ") % (diff[1], )))
        else:
            html.append(macro.formatter.text(_(" %dm ") % (diff[2], )))
        html.append(macro.formatter.text(_(" ago.")))
        html.append(macro.formatter.listitem(0))
    html.append(macro.formatter.number_list(0))
    html.append('</div>')
    
    return macro.formatter.rawHTML("".join(html))

def time_diff(t1, t2):
    t = t1 - t2
    t,s = divmod(t,60)
    t,m = divmod(t,60)
    d,h = divmod(t,24)
    return (d,h,m,s)
