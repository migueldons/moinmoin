# -*- coding: iso-8859-1 -*-
"""
Parser module for MoinMoin
                     
----
Copyright (C) 2007-2008 Zoran Isailovski
Licensed under the Apache License, Version 2.0 (see http://www.apache.org/licenses/LICENSE-2.0)
or, at your option, under the terms described in the file LICENSE.txt found in the root of the 
distribution of this package (as far as it denotes a different license then the above).
"""

from MoinGraphViz import main

class Parser:
    """
        Draw graphs using the GraphViz toolset.
    """
##    extensions = [] # extensions = '*' to cover attachments, too
##    Dependencies = []
    def __init__(self, raw, request, **kw):
        if main.DEBUG: reload(main)
        self.delegate = p = main.Parser(raw, request, **kw)
        self.extensions = p.extensions
        self.Dependencies = p.Dependencies
    def format(self, formatter):
        return self.delegate.format(formatter)
