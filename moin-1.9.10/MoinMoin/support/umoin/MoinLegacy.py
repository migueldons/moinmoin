try:
    from MoinMoin.parser import wiki
except ImportError:
    from MoinMoin.parser import text_moin_wiki as wiki # moin 1.6+

######################################################################
## 
## Backwards Compatibility Functions
## 
######################################################################

def send_title(request, text, **keywords):
    return request.theme.send_title(text, **keywords)

def send_footer(request, pagename, **keywords): 
    return request.theme.send_footer(pagename, **keywords)

######################################################################
## 
## Backwards Compatibility Injection
## 
######################################################################

def inject_backward_compatibility():
    def install_method(method, target):
        key = method.__name__
        if not hasattr(target, key): setattr(target, key, method)

    from MoinMoin import wikiutil, request

    for method in (send_title, send_footer):
        install_method(method, wikiutil)

    req = request.RequestBase
    if not hasattr(req, 'http_headers') and hasattr(req, 'emit_http_headers'):
        req.http_headers = req.emit_http_headers

inject_backward_compatibility()

######################################################################
## 
## MAIN - run wiki server
## 
######################################################################

if __name__ == '__main__':
    #from moin import Config, run
    #run(Config)
    ##execfile('wikiserver.py')
    execfile('moin.py')
